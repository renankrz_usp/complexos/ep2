# pingr

## Back end

Use port 3001.

```bash
$ npx json-server-auth db.json --port 3001
```

## Build Setup

```bash
# install dependencies
$ npm install

# serve with hot reload at localhost:3000
$ npm run dev

# build for production and launch server
$ npm run build
$ npm run start

# generate static project
$ npm run generate
```
