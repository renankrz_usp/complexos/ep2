import jwt from 'jsonwebtoken';

export const state = () => ({
    token: null,
    userId: null,
});

export const mutations = {
    set(state, token) {
        state.token = token;
        state.userId = jwt.decode(token).sub;
    },
};
